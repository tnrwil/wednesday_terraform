provider "digitalocean" {
  token = "8b7db0c4cb06711518451c00ad98fbd61c895b8029fdec166b612bd23cc70ad1"
}

#data "digitalocean_droplet" "droplets_created" {
#  tag = "demo-2"
#}



#Webservers that will have docker on them. (containers)
resource "digitalocean_droplet" "test_demo" {
  image      = var.image
  name       = var.name
  region     = var.region
  size       = var.size
  backups    = var.backups
  monitoring = var.monitoring
  count      = 4 
  tags = [
    "demo-${count.index + 1}",
    "centos-8",
    "classoncloud",
    "wil"
  ]
}

