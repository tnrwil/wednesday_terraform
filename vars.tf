variable "name" {
  default = "tameika-demo"
}

variable "image" {
  default = "centos-8-x64"
}

variable "region" {
  default = "nyc3"
}
variable "size" {
  default = "s-1vcpu-2gb"
}
variable "backups" {
  default = "true"
}

variable "monitoring" {
  default = "true"
}

#variable "ssh_keys" {
#  default="cie-mis"
#}

variable "tag" {
  type    = list
  default = ["test_deployment"]
}

#Memcache variables 
variable "mem_name" {
  default = "tameika-mem"
}

variable "mem_image" {
  default = ""
}

variable "mem_region" {
  default = "nyc3"
}
variable "mem_size" {
  default = "s-1vcpu-2gb"
}
variable "mem_backups" {
  default = "true"
}

variable "mem_monitoring" {
  default = "true"
}

#variable "ssh_keys" {
#  default="cie-mis"
#}


